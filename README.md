
# Atelier SI24

Support de présentation d'un atelier de découverte de services libres, pendant un TD de SI24 en P19.

### Utilisation de la présentation et modification des slides

1. Télécharger le code source
2. Extraire le dossier compressé .zip 
3. Ouvrir index.html dans un navigateur pour voir la présentation, modifier ce même fichier pour toute amélioration \o/


### License

Réalisé avec RevealJs
MIT licensed
Copyright (C) 2019 Hakim El Hattab, http://hakim.se
